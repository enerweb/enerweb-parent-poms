<project
  xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd"
>
  <modelVersion>4.0.0</modelVersion>
  <groupId>za.co.enerweb</groupId>
  <artifactId>public-corporate-pom</artifactId>
  <version>1.1.2-SNAPSHOT</version>
  <packaging>pom</packaging>
  <name>Enerweb Public Corporate pom</name>
  <description>Common maven configs which all
  Enerweb maven projects should inherit from.</description>
  <organization>
    <name>Enerweb</name>
    <url>http://www.enerweb.co.za/</url>
  </organization>
  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

    <ew.deploy-public>true</ew.deploy-public>
    <ew.mavenrepo>ew.mavenrepo</ew.mavenrepo>
    <ew.repository-base>https://maven.enerweb.co.za/mavenrepo/content/repositories</ew.repository-base>
    <ew.public-releases-repository>${ew.repository-base}/public-releases</ew.public-releases-repository>
    <ew.public-snapshots-repository>${ew.repository-base}/public-snapshots</ew.public-snapshots-repository>
    <ew.public-site-repository>${ew.repository-base}/public-site</ew.public-site-repository>
    <ew.private-releases-repository>${ew.repository-base}/releases</ew.private-releases-repository>
    <ew.private-snapshots-repository>${ew.repository-base}/snapshots</ew.private-snapshots-repository>
    <ew.private-site-repository>${ew.repository-base}/site</ew.private-site-repository>

    <!-- Give a better error message if you are deploying without the right
    profile enabled. We need to do this to be very sure people don't
    accidentally deploy stuff publicly that is not public.-->
    <ew.deploy.message>NB!&#10;&#10;&#10;
      NB! REMEMBER TO SPECIFY -Dew.deploy-public TO
      DEPLOY THIS PUBLICLY OR -Dew.deploy-private TO DEPLOY THIS
      PRIVATELY&#10;&#10;&#10;Don't say I didn't try to tell you!!!!?!
    </ew.deploy.message>
    <ew.snapshots-repository>${ew.private-snapshots-repository} OR
      ${ew.public-snapshots-repository}
      ${ew.deploy.message}
    </ew.snapshots-repository>
    <ew.releases-repository>${ew.private-releases-repository} OR
      ${ew.public-releases-repository}
      ${ew.deploy.message}</ew.releases-repository>
    <ew.site-repository>${ew.private-site-repository} OR
      ${ew.public-site-repository}
      ${ew.deploy.message}</ew.site-repository>

    <maven-project-info-reports-plugin.version>2.6</maven-project-info-reports-plugin.version>
  </properties>
  <build>
    <defaultGoal>install</defaultGoal>
    <extensions>
      <extension>
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-webdav-jackrabbit</artifactId>
        <version>2.2</version>
      </extension>
    </extensions>
    <plugins>
      <!-- added for threadsafety -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
        <version>2.7</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-site-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>${maven-project-info-reports-plugin.version}</version>
        <configuration>
          <dependencyDetailsEnabled>false</dependencyDetailsEnabled>
          <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
        </configuration>
      </plugin>
    </plugins>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-clean-plugin</artifactId>
          <version>2.5</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>2.3.1</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <version>3.4</version>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-project-info-reports-plugin</artifactId>
           <version>2.7</version>
          <!-- <version>2.6</version> -->
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>${maven-project-info-reports-plugin.version}</version>
        <configuration>
          <dependencyDetailsEnabled>false</dependencyDetailsEnabled>
          <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
        </configuration>
      </plugin>
    </plugins>
  </reporting>
  <distributionManagement>
    <repository>
      <id>ew.mavenrepo</id>
      <name>enerweb.releases-repository</name>
      <url>${ew.releases-repository}</url>
    </repository>
    <snapshotRepository>
      <id>ew.mavenrepo</id>
      <name>enerweb.snapshots-repository</name>
      <url>${ew.snapshots-repository}</url>
    </snapshotRepository>
    <site>
      <id>ew.mavenrepo</id>
      <name>enerweb.site-repository</name>
      <!-- <url>${ew.site-repository}/${project.artifactId}-${project.version}</url> -->
      <url>${ew.site-repository}</url>
    </site>
  </distributionManagement>
  <profiles>
    <profile>
      <id>ew.deploy-public</id>
      <activation>
        <property>
          <name>ew.deploy-public</name>
          <value>true</value>
        </property>
      </activation>
      <properties>
        <ew.snapshots-repository>${ew.public-snapshots-repository}</ew.snapshots-repository>
        <ew.releases-repository>${ew.public-releases-repository}</ew.releases-repository>
        <ew.site-repository>${ew.public-site-repository}</ew.site-repository>
      </properties>
    </profile>
    <profile>
      <id>ew.deploy-private</id>
      <activation>
        <property>
          <name>ew.deploy-private</name>
          <value>true</value>
        </property>
      </activation>
      <properties>
        <ew.snapshots-repository>${ew.private-snapshots-repository}</ew.snapshots-repository>
        <ew.releases-repository>${ew.private-releases-repository}</ew.releases-repository>
        <ew.site-repository>${ew.private-site-repository}</ew.site-repository>
      </properties>
    </profile>
  </profiles>
</project>
